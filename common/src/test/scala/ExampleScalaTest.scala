import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

/**
 * A small example test, showing how to use FlatSpec based scala testing.
 */
@RunWith(classOf[JUnitRunner])
class ExampleScalaTest extends FlatSpec {

  "Example test" should "run test" in {
    assert(0 == 0)
  }

  // a failing test.
//  it should "fail" in {
//    assert(1 == 0)
//  }
}
